# shamstore

ONLY FOR ACADEMIC REASON, DO NOT USE IT.

_shamstore_ is a set of programs to create and store shares for further distribution.

It consists of two executables.

## shamwrite

_shamwrite_ is used to create shares based on a file and store those as files.

### usage

It has three parameter

1. --from the path of the file to share 
2. --to comma separated list of paths to store the shares
3. --min the minmal amount of shares needed to restore the data (the default is 2)

E.g.

```
./shamwrite --from README.md --to /tmp/r1,/tmp/r2,/tmp/r3,/tmp/r4,/tmp/r5,/tmp/r6 --min 3
```

## shamread

_shamread_ is used to restore a file based on the shares.

### usage
It has two parameter

1. --from a comma separated list of paths for shares
2. --to a path to store the recovered date

E.g.
```
./shamread --from /tmp/r1,/tmp/r2,/tmp/r5 --to /tmp/recovered.md
```
