package storage

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/nichtsfrei/shamstore/pkg/sss"
)

// Store is a function to store data, with a minimal amount of shared to an array of io.Writer
type Store func(int, []byte, []io.Writer) error

func guardStores(stores []io.Writer) error {
	for i, s := range stores {
		if s == nil {
			return fmt.Errorf("store index %d is nil, validate io.Writer creation", i)
		}
	}
	return nil
}

// StoreWith is used to create a guarded Store function with the given
// sss.GenerateShares, crypt.EncryptData as well as an errorHandler to
// manipulate the behaviour on an error.
func StoreWith(
	generateShares sss.GenerateShares,
	handleStorageError func(error) error) Store {
	return func(minimumShares int, data []byte, stores []io.Writer) error {
		if err := guardStores(stores); err != nil {
			return err
		}
		amountShares := len(stores)
		if amountShares < minimumShares {
			return fmt.Errorf("the amount of available storages (%d) is smaller then the wanted required minimum (%d) to restore the secret", amountShares, minimumShares)
		}
		shares, err := generateShares(data, minimumShares, amountShares)
		if err != nil {
			return err
		}
		for i, s := range shares {
			c := stored{
				Shares: s,
			}

			bytes, err := json.Marshal(&c)
			if err == nil {
				_, err = stores[i].Write(bytes)
			}
			if handleStorageError(err) != nil {
				return err
			}

		}
		return nil
	}
}

func abortOnError(e error) error {
	return e
}

var defaultStore = StoreWith(sss.Share,
	abortOnError)

// DefaultStore uses a Store function with sss.Share, crypt.Encrypt as well as abort on error
func DefaultStore(minimumShares int, data []byte, stores []io.Writer) error {
	return defaultStore(minimumShares, data, stores)
}
