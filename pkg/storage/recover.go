package storage

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/nichtsfrei/shamstore/pkg/sss"
)

//Recover is used to recover secrets via the given readers
type Recover func([]io.Reader) ([]byte, error)

//RecoverWith is used to create a Recover function with the given
// sss.Recovery, crypt.DecryptData as well as an errorHandler.
func RecoverWith(recover sss.RecoverFromShares,
	handleStorageError func(error) error) Recover {

	return func(reader []io.Reader) ([]byte, error) {
		results := make([][]int, len(reader))
		for i, r := range reader {
			// TODO change to channel approach to speed things up ...
			dec := json.NewDecoder(r)
			var c stored
			err := dec.Decode(&c)
			if err != nil {
				if handleStorageError(err) != nil {
					return nil, err
				}
			} else {
				results[i] = c.Shares
			}
		}
		rawCombined := recover(results)
		return rawCombined, nil
	}
}
func ignoreError(e error) error {
	if e != nil {
		fmt.Printf("WARNING: ignoring an error while reading data: %v\n", e)
	}
	return nil
}

var defaultRecover = RecoverWith(sss.Recover,
	ignoreError)

// DefaultRecover is Recover function with sss.Recover, crypt.Decrypt as well as ignoring errors on read
var DefaultRecover Recover = func(reader []io.Reader) ([]byte, error) {
	return defaultRecover(reader)
}
