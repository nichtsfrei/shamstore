package storage

import (
	"bytes"
	"io"
	"testing"
)

func TestDefaultStore(t *testing.T) {
	type args struct {
		minimumShares int
		data          []byte
		stores        []io.ReadWriter
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "should be able to recover",
			args: args{
				minimumShares: 2,
				data:          []byte("hallo"),
				stores:        []io.ReadWriter{new(bytes.Buffer), new(bytes.Buffer), new(bytes.Buffer)},
			},
		},
		{
			name: "should return error if stores has a nil reference",
			args: args{
				minimumShares: 2,
				data:          []byte("hallo"),
				stores:        []io.ReadWriter{new(bytes.Buffer), nil, new(bytes.Buffer)},
			},
			wantErr: true,
		},
		{
			name: "should return error if not enough stores",
			args: args{
				minimumShares: 4,
				data:          []byte("hallo"),
				stores:        []io.ReadWriter{new(bytes.Buffer), new(bytes.Buffer), new(bytes.Buffer)},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writer := make([]io.Writer, len(tt.args.stores))
			for i, w := range tt.args.stores {
				writer[i] = w
			}
			if err := DefaultStore(tt.args.minimumShares, tt.args.data, writer); (err != nil) != tt.wantErr {
				t.Errorf("DefaultStore() error = %v, wantErr %v", err, tt.wantErr)
			} else {
				if !tt.wantErr {
					reader := make([]io.Reader, tt.args.minimumShares)
					for i, r := range tt.args.stores {
						if i >= tt.args.minimumShares {
							break
						}
						reader[i] = r
					}
					data, err := DefaultRecover(reader)
					if err != nil {
						t.Errorf("DefaultRecover() error = %v, wantErr %v", err, tt.wantErr)
					}
					if string(data) != string(tt.args.data) {
						t.Errorf("DefaultRecover() data = %v, expected: %v", string(data), string(tt.args.data))
					}
				}
			}
		})
	}
}
