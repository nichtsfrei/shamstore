package sss

import (
	"crypto/rand"
	"math/big"
)

const DEFAULT_PRIME = 257

type GenerateShares func([]byte, int, int) ([][]int, error)
type RecoverFromShares func([][]int) []byte
type indexedShare struct {
	Index int
	Share int
}

func mod(a, b int) int {
	return (a%b + b) % b
}

func generateShares(secret int, minimum int, shares int, prime int) ([]indexedShare, error) {
	evaluate_polynomial := func(poly []int, x int) indexedShare {
		accumilator := 0
		for i := len(poly) - 1; i >= 0; i-- {
			accumilator *= x
			accumilator += poly[i]
			accumilator = mod(accumilator, prime)
		}
		return indexedShare{x, accumilator}
	}
	polies := make([]int, minimum)
	polies[0] = secret
	for i := 1; i < minimum; i++ {
		nBig, err := rand.Int(rand.Reader, big.NewInt(int64(prime-1)))
		if err != nil {
			return nil, err
		}
		polies[i] = int(nBig.Int64())
	}
	result := make([]indexedShare, shares)
	for i := 0; i < shares; i++ {
		result[i] = evaluate_polynomial(polies, i+1)
	}
	return result, nil
}

func recoverSecret(shares []indexedShare, prime int) int {
	numDenPrime := func(num, den, p int) int {
		x := 0
		last := 1
		for p != 0 {
			last, x = x, last-(den/p)*x
			den, p = p, mod(den, p)
		}
		return num * last
	}
	den := 1
	nums := make([]int, len(shares))
	dens := make([]int, len(shares))

	for i := 0; i < len(shares); i++ {
		cur := shares[i].Index
		denToPush := 1
		numToPush := 1
		for j := 0; j < len(shares); j++ {
			if i != j {
				x := shares[j].Index
				denToPush = mod((cur-x)*denToPush, prime)
				numToPush = mod((0-x)*numToPush, prime)
			}
		}
		den = mod(denToPush*den, prime)
		dens[i] = denToPush
		nums[i] = numToPush
	}

	num := 0
	for i := 0; i < len(shares); i++ {
		num += numDenPrime(mod(shares[i].Share*den*nums[i], prime), dens[i], prime)
	}
	return mod(numDenPrime(num, den, prime)+prime, prime)
}

var Share GenerateShares = func(src []byte, minimum, amountShares int) ([][]int, error) {
	result := make([][]int, amountShares)
	for i := 0; i < amountShares; i++ {
		s := make([]int, len(src)+1)
		s[0] = i + 1
		result[i] = s
	}
	for i, b := range src {
		pairs, err := generateShares(int(b), minimum, amountShares, DEFAULT_PRIME)
		if err != nil {
			return nil, err
		}
		for j, p := range pairs {
			result[j][i+1] = p.Share
		}
	}
	return result, nil
}

var Recover RecoverFromShares = func(src [][]int) []byte {
	if len(src) == 0 {
		return make([]byte, 0)
	}
	amountOfBytes := len(src[0]) - 1
	result := make([]byte, amountOfBytes)
	for i := 0; i < amountOfBytes; i++ {
		collected := make([]indexedShare, len(src))
		for j := 0; j < len(src); j++ {
			collected[j] = indexedShare{src[j][0], src[j][i+1]}
		}
		result[i] = byte(recoverSecret(collected, DEFAULT_PRIME))
	}
	return result
}
