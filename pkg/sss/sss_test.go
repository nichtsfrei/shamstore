package sss

import (
	"math/rand"
	"reflect"
	"testing"
)

func TestShouldBeAbleToShareAndRecoverBytes(t *testing.T) {
	tests := []struct {
		name             string
		manipulateShares func([][]int) [][]int
		fail             bool
		secret           []byte
	}{
		{
			name:   "1. should recover it with all parts",
			secret: []byte{1, 2, 3, 4, 5, 6},
		},
		{
			name:   "2. should recover it with 3 parts",
			secret: []byte{1, 2, 3, 4, 5, 6},
			manipulateShares: func(slice [][]int) [][]int {
				for i := range slice {
					j := rand.Intn(i + 1)
					slice[i], slice[j] = slice[j], slice[i]
				}
				return slice[0:3]
			},
		},
		{
			name:   "3. should not recover it with 2 parts",
			secret: []byte{1, 2, 3, 4, 5, 6},
			manipulateShares: func(slice [][]int) [][]int {
				for i := range slice {
					j := rand.Intn(i + 1)
					slice[i], slice[j] = slice[j], slice[i]
				}
				return slice[0:2]
			},
			fail: true,
		},
		{
			name:   "4. should not recover it empty slice",
			secret: []byte{1, 2, 3, 4, 5, 6},
			manipulateShares: func(slice [][]int) [][]int {
				return make([][]int, 0)
			},
			fail: true,
		},
		{
			name:   "5. should not recover it on nil",
			secret: []byte{1, 2, 3, 4, 5, 6},
			manipulateShares: func(slice [][]int) [][]int {
				return nil
			},
			fail: true,
		},
		{
			name: "6. should return gibberish on nil src",
			manipulateShares: func(slice [][]int) [][]int {
				if len(slice) != 6 {
					t.Errorf("expected 6 shares but got %v", slice)
				}
				return slice
			},
			fail: true,
		},
	}
	for _, tt := range tests {
		shares, _ := Share(tt.secret, 3, 6)
		if tt.manipulateShares != nil {
			shares = tt.manipulateShares(shares)
		}
		recovered := Recover(shares)
		if reflect.DeepEqual(recovered, tt.secret) != !tt.fail {
			t.Errorf("%s failed, %v expected %v and %v to not be %v", tt.name, shares, recovered, tt.secret, tt.fail)
		}
	}

}
