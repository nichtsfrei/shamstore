package main

import (
	"flag"
	"io"
	"os"
	"strings"

	"gitlab.com/nichtsfrei/shamstore/pkg/storage"
)

func main() {
	fromArg := flag.String("from", "", "comma separated list of paths to read from")
	to := flag.String("to", "", "path to store to")
	flag.Parse()
	from := strings.Split(*fromArg, ",")
	reader := make([]io.Reader, len(from))
	for i, s := range from {
		f, err := os.Open(s)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		reader[i] = f
	}
	data, err := storage.DefaultRecover(reader)
	if err != nil {
		panic(err)
	}
	store, err := os.Create(*to)
	if err != nil {
		panic(err)
	}
	store.Write(data)
}
