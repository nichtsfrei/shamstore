package main

import (
	"flag"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/nichtsfrei/shamstore/pkg/storage"
)

func main() {
	from := flag.String("from", "", "path of the file to encrypt")
	toArg := flag.String("to", "", "comma separated list of paths to store to")
	min := flag.Int("min", 2, "the minimal amount of shared needed to restore the secret. Default is 2")
	flag.Parse()
	b, err := ioutil.ReadFile(*from)
	if err != nil {
		panic(err)
	}
	to := strings.Split(*toArg, ",")
	writer := make([]io.Writer, len(to))
	for i, s := range to {
		f, err := os.Create(s)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		writer[i] = f
	}
	err = storage.DefaultStore(*min, b, writer)
	if err != nil {
		panic(err)
	}
}
