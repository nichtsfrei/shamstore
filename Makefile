LOCAL_OS := $(shell uname)
ifeq ($(LOCAL_OS),Linux)
	TARGET_OS_LOCAL = linux
else ifeq ($(LOCAL_OS),Darwin)
	TARGET_OS_LOCAL = darwin
else
	TARGET_OS_LOCAL ?= windows
endif
export GOOS ?= $(TARGET_OS_LOCAL)

ifeq ($(GOOS),windows)
	BINARY_EXT_LOCAL:=.exe
else
	BINARY_EXT_LOCAL:=
endif

export BINARY_EXT ?= $(BINARY_EXT_LOCAL)

define build
	mkdir -p dst/$(1);
	GOOS=$(1) go build -a -o dst/$(1)/shamread$(2) cmd/shamread/main.go; 
	GOOS=$(1) go build -a -o dst/$(1)/shamwrite$(2) cmd/shamwrite/main.go; 
endef

all: test build
build:
	$(call build,$(GOOS), $(BINARY_EXT))
release:
	$(call build,linux)
	$(call build,windows,.exe)
	$(call build,darwin)
test:
	go test ./...
